variable "project-id" {
    default = "boris-zamora"
    type = string
}
variable "region" {
    default = "us-east1"
    type = string
}
variable "zone" {
    type = string
    default = "us-east1-b"
}
variable "vpc-name" {
    type = string
    default = "default"
}