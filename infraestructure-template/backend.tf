terraform {
    backend "gcs" {
        credentials = "./creds/asure-infra-creator.json"
        bucket = "pipeline-infraestructure"
        prefix = "pipeline-infraestructure-state"
    }
}
