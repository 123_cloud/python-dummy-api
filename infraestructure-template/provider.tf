provider "google" {
  credentials = "${file("./creds/asure-infra-creator.json")}"
  project = "boris-zamora"
  region = "us-east1"
  zone = "us-east1-b"
}
